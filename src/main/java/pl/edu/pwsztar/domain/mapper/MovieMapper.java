package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieMapper implements Converter<CreateMovieDto, Movie>{

    static Converter<CreateMovieDto, Movie> convertToEntity = (movie) ->
            new Movie(movie.getTitle(), movie.getImage(), movie.getYear());

    @Override
    public Movie convert(CreateMovieDto from) {
        return convertToEntity.convert(from);
    }
}
